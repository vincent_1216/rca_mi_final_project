async function submitServiceRequest(){
var  patientIdentity = document.getElementById('identity').value;
var  doctorId = document.getElementById('doctorName').value;
var  patientId = document.getElementById('patientName').value;
var  TreatmentGoals = document.getElementById('TreatmentGoals').value;
var  TreatmentType = document.getElementById('TreatmentType').value;
var  TreatmentCode = document.getElementById('TreatmentCode').value;
var  TreatmentFrequency = document.getElementById('TreatmentFrequency').value;
var  treatStartDate = document.getElementById('patientName').value;
var  treatEndDate = document.getElementById('patientName').value;
var  drugs = document.getElementById('drugs').value;
var  drugsCode = document.getElementById('drugsCode').value;
var  MedicalDevices = document.getElementById('MedicalDevices').value;
var  SpecialInstructions = document.getElementById('SpecialInstructions').value;
var  note = document.getElementById('note').value;
var  frequencyUnit = document.getElementById('frequencyUnit').value;
var encounterId;
var deviceId;
var serviceRequestData = {
    "resourceType": "ServiceRequest",
    "identifier": [
      {
        "use": "official",
        "value": patientIdentity
      }
    ],
    "status": "active",
    "intent": "order",
    "subject": {
      "reference": "Patient/" + patientId
    },
    "encounter": {
      "reference": "Encounter/" + encounterId
    },
    "authoredOn": treatStartDate,
    "requester": {
      "reference": "Practitioner/" + doctorId
    },
    "code": {
      "coding": [
        {
          "system": "http://hl7.org/fhir/CodeSystem/treatment",
          "code": TreatmentCode,
          "display": TreatmentType
        }
      ],
      "text": TreatmentGoals
    },
    "quantityQuantity": {
      "value": TreatmentFrequency,
      "unit": frequencyUnit
    },
    "occurrencePeriod": {
      "start": treatStartDate,
      "end": treatEndDate
    },
    "medicationCodeableConcept": {
      "coding": [
        {
          "system": "http://hl7.org/fhir/medication-code",
          "code": drugsCode,
          "display": drugs
        }
      ]
    },
    "device": [
      {
        "reference": "Device/" + deviceId
      }
    ],
    "note": [
      {
        "text": note
      }
    ]
  };
  
  function postServiceRequest() {
      fetch('https://hapi.fhir.org/baseR4/ServiceRequest', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/fhir+json'
          },
          body: JSON.stringify(serviceRequestData)
      })
      .then(response => response.json())
      .then(data => {
          console.log('ServiceRequest created successfully:', data);
          alert('ServiceRequest created successfully. ID: ' + data.id);
      })
      .catch((error) => {
          console.error('Error:', error);
          alert('Error creating ServiceRequest');
      });
  }
}